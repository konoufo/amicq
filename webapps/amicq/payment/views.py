from django.http import Http404
from django.shortcuts import render
from django.template.context_processors import csrf
from django.conf import settings
import stripe
import math


def donation(request):

    if request.method == 'GET':
        stripe.api_key = settings.STRIPE_SECRET_KEY
        c = {'key': settings.STRIPE_PUBLISHABLE_KEY,
             }
        c.update(csrf(request))
        return render(request, 'payment/payments.html',c)

    elif request.method == 'POST':
        token = request.POST.get('stripeToken')
        payment_amount = request.POST.get('paymentAmount')
        stripe.api_key = settings.STRIPE_SECRET_KEY
        if token and payment_amount:
            try:

                #membership_fees = int(settings.MEMBERSHIP_FEES)*100
                charge = stripe.Charge.create(
                    amount=int(math.ceil(payment_amount)), # amount in cents, again
                    currency="cad",
                    card=token,
                    description="donation"
                )

                #request.user.paid=True
                #request.user.save()
                #certimail_payment.save()
                #send_payment_email(request, diagnostic_fees)
                #generate_invoice(request.user, float(diagnostic_fees)/100)

                return render(request, 'payment_succeeded.html')
            except stripe.InvalidRequestError, stripe.CardError:
                pass
        return render(request, 'payment_failed.html')
    else:
        raise Http404
