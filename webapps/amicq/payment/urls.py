from payment import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.donation, name="home"),
]