from django.conf.urls import url
import views


urlpatterns = [
    url(r'^$', views.Home.as_view(), name="home"),
    url(r'^statuts/$', views.statuts, name="statuts"),
    url(r'^annonces/$', views.annonces, name="annonces"),
    url(r'^joindre/$', views.joindre, name="joindre"),
    url(r'^mission/$', views.mission, name="mission"),
    url(r'^formulaire/$', views.formulaire, name="formulaire"),
    url(r'^nouvelles/$', views.news, name="news"),
    url(r'^telechargement/statuts/$', views.download, name="download_statuts")
]