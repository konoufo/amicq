import os

from django.shortcuts import redirect, render
from django.views.generic import View
from django.conf import settings
from django.http import HttpResponse


class Home(View):
    def get(self, request):
        return render(request, 'home/index.html')


def statuts(request):
    return render(request, 'home/StatutsRI.html')


def joindre(request):
    return render(request, 'home/Joindre.html')


def annonces(request):
    return render(request, 'home/annonces.html')


def mission(request):
    return render(request, 'home/mission.html')


def formulaire(request):
    return render(request, 'home/formulaire.html')


def download(request):
    path_to = os.path.join(settings.BASE_DIR, 'downloads', 'statuts.pdf')
    with open(path_to, 'r') as f:
        resp = HttpResponse(f.read(), content_type='application/pdf')
    resp['Content-Disposition'] = 'attachment; filename="AMICQ Statuts.pdf"'
    return resp


def news(request):
    return render(request, 'home/nouvelles.html')
