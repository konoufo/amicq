# Settings file for the production environment.
# It essentially consist in whatever parameters i need to have different from the ones
# in a development environment. Modify accordingly.
import os
import urlparse

PRODUCTION = False # Don't TOUCH this one, please !

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'amicq$amicqpay',
        'HOST': 'amicq.mysql.pythonanywhere-services.com',
        'USER':'amicq',
        'PASSWORD':'$h1n$3k@1',
        'PORT':''
    }
}
if not os.environ.get('SAFE_PRIVATE_DJANGO'):
    PRODUCTION = True
    if os.environ.get('OPENSHIFT_POSTGRESQL_DB_URL') is not None:
        db_url = urlparse.urlparse(os.environ.get('OPENSHIFT_POSTGRESQL_DB_URL'))
        if db_url:
            DATABASES = {
                'default': {
                    'ENGINE': 'django.db.backends.postgresql_psycopg2',
                    'NAME': os.environ['OPENSHIFT_APP_NAME'],
                    'USER': db_url.username,
                    'PASSWORD': db_url.password,
                    'HOST': db_url.hostname,
                    'PORT': db_url.port,
                }
            }

