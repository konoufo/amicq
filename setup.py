from setuptools import setup

with open('requirements.txt', 'r') as f:
      lines = f.readlines()
setup(name='AmicqWeb',
      version='1.1.2',
      description='OpenShift App',
      author='konoufo',
      author_email='konoufo@hotmail.fr',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=lines,
)
